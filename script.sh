#!/bin/bash
#This script will do the basic setup for Oracle VPS which runs Ubuntu 18.04

#Let's update and upgrade the system
sudo apt update -y && sudo apt upgrade -y;

#Let's make a swap space
sudo fallocate -l 2G /swapfile;
#You can change the file size. I have used 2 Gigabytes
sudo chmod 600 /swapfile;
#Changing permissions for swap file
sudo mkswap /swapfile;
#Mark swap file
sudo swapon /swapfile;
#Allow system to utilize that swap space
sudo cp /etc/fstab /etc/fstab.bak;
#It will backup the fstab file for safety
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab;
#It should mod fstab file for make it permanant

#Let's setup iptable rules
sudo iptables -D INPUT 6;
#This will delete the annoying rule of iptables
sudo netfilter-persistent save;
#For save current iptable rules
sudo netfilter-persistent reload;
#Finally we reload iptable rules

#Let's reboot our instance!
sudo reboot;
